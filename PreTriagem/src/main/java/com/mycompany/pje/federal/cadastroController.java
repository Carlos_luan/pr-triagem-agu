/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author Edu
 */
public class cadastroController implements Initializable{
@FXML
    TableView<Chaves> tablebank;
    @FXML
    TableColumn<Chaves, String> NucleoColuna;
    @FXML
    TableColumn<Chaves, String> RegiaoColunm;
    @FXML
    TableColumn<Chaves, String> instanciacolunm;
    @FXML
    TableColumn<Chaves, String> pesocolunm;
 
 
   @FXML
    void RetornaMenu(ActionEvent event)
    {
        Node node = (Node) event.getSource();

        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        try
        {
            root = FXMLLoader.load(getClass().getResource("/fxml/Root.fxml"));
        }
        catch (IOException ex)
        {

        }
        Scene scene = new Scene(root);
        stage.setMinWidth(400);
        stage.setMinHeight(400);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
        stage.setTitle("Mark-II");
    }
    
    @FXML
    void JanelaAdd() throws IOException{
        //FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditVF.fxml"));
        //loader.setController(new EditController(chave));
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/EtiquetaController.fxml"));;
        Stage stage = new Stage();
        stage.setTitle("Editar Chaves");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(new Scene(root));
        stage.show();
    }
    
    @FXML
    void JanelaAdd2() throws IOException{
    Chaves chave = new Chaves();
        chave.setNucleo(tablebank.getSelectionModel().getSelectedItem().getNucleo().replace("'", "").replace("´", ""));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ParamNucleo.fxml"));
        loader.setController(new EditController(chave));
        //Parent root = FXMLLoader.load(getClass().getResource("/fxml/ParamNucleo.fxml"));
        Parent root = loader.load();
        Stage stage = new Stage();
        stage.setTitle("Configurar Etiqueta");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(new Scene(root));
        stage.show();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        try
        {

            List<Chaves> chaves = new ArrayList<>();
            Connection connection = DriverManager.getConnection("jdbc:sqlite:Pre_triagem.db");
            // lendo os registros
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM NUCLEO ORDER BY PESO");
            ResultSet resultSet = stmt.executeQuery();
            int i = 0;
            while (resultSet.next())
            {
                Chaves key = new Chaves();
                key.setNucleo(resultSet.getString("ETIQUETA"));
                key.setPeso(resultSet.getString("PESO"));
                key.setInstancia(resultSet.getString("INSTANCIA"));
                key.setRegiao(resultSet.getString("REGIAO"));
                chaves.add(key);
                i++;
            }

            NucleoColuna.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Nucleo"));
            RegiaoColunm.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Regiao"));
            instanciacolunm.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Instancia"));
            pesocolunm.setCellValueFactory(new PropertyValueFactory<Chaves, String>("Peso"));
            ObservableList<Chaves> genericos = FXCollections.observableArrayList(chaves);
            tablebank.setItems(genericos);
            connection.close();

        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

}
}
