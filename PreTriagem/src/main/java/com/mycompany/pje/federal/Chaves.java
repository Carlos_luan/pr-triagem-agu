/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

/**
 *
 * @author Edu
 */
import javax.swing.JOptionPane;

/**
 *
 * @author carlos.luan
 */
public class Chaves {

    private String Nucleo = null;
    private String Regiao = null;
    private String Instancia = null;
    private String Peso = null;

    public String getNucleo() {
        return Nucleo;
    }

    public void setNucleo(String Nucleo) {
        this.Nucleo = Nucleo;
    }

    public String getRegiao() {
        return Regiao;
    }

    public void setRegiao(String Regiao) {
        this.Regiao = Regiao;
    }

    public String getInstancia() {
        return Instancia;
    }

    public void setInstancia(String Instancia) {
        this.Instancia = Instancia;
    }

    public String getPeso() {
        return Peso;
    }

    public void setPeso(String Peso) {
        this.Peso = Peso;
    }

}
