/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Edu
 */
public class ParamController implements Initializable {

    @FXML
    JFXTextField etiqTxt;
    @FXML
    JFXComboBox<String> regiao;
    @FXML
    JFXComboBox<String> instancia;
    @FXML
    JFXComboBox<String> peso;
    @FXML
    JFXComboBox<String> campos;

    BancoController banco = new BancoController();
    PreparedStatement Regiao = banco.SelectRegiao();

//abre janela de loading
    void loadingJanel() throws IOException {
        //FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EditVF.fxml"));
        //loader.setController(new EditController(chave));
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Progress.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Configurar Etiqueta");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(new Scene(root));
        stage.show();
    }

    @FXML
    public void cancelar(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        stage.close();
    }

    @FXML
    public void inserirEtiqueta() {
        try {
            
            BancoController banco2 = new BancoController();
            final String nucleo = etiqTxt.getText().trim().replaceAll("'", "");
            int reg = 0;
            PreparedStatement registro = banco.SelectRegiaoSet(regiao.getValue());
            ResultSet regi = registro.executeQuery();
            while (regi.next()) {
                reg = regi.getInt("ID_REGIAO");
            }
            final int regiao = reg;
            registro.close();
            if (nucleo.isEmpty()) {
                // lembrar de colocar um aviso pra não serem trouxas
            } else {

                boolean check = banco2.inserirNucleo(nucleo,
                        Integer.parseInt(peso.getValue()),
                        reg,
                        Integer.parseInt(instancia.getValue()),
                        Integer.parseInt(campos.getValue()));// verifico se a informação foi armazenada no banco de
                // dados
                if (check) {
                    Runnable task = new Runnable() {
                        public void run() {
                            configurar(nucleo, regiao, instancia.getValue()); // TODO Auto-generated catch block
                        }
                    };
                    // Run the task in a background thread
                    Thread backgroundThread = new Thread(task);
//        // Terminate the running thread if the application exits
                    backgroundThread.setDaemon(true);
//        // Start the thread
                    backgroundThread.start();
                    loadingJanel();

                }
            }
            System.out.print("\nFui Clicado");

        } catch (Exception ex) {
            Logger.getLogger(ParamController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void configurar(String nucleo, int reg, String instancia) {
        try {
            //cria o objeto pra setar o progresso
//            LoadingController loadp = new LoadingController();
            //cria uma nova trhead pra janela
            //        loadp.startTask();
//             loadp.setProgresso();
            List<String> Assunto = banco.SelectAssuntoID();
            System.out.println("Comecei Assunto" + System.currentTimeMillis());
            for (int i = 0; i < Assunto.size(); i++) {
                banco.inserirConfig(nucleo, Assunto.get(i), "ASSUNTO");
            }
            System.gc();
            //seta o progresso dessa etapa
//            loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
            List<String> Unidade = banco.SelectUnidadeID();
            for (int i = 0; i < Unidade.size(); i++) {
                banco.inserirConfig(nucleo, Unidade.get(i), "UNIDADE");
            }
            System.gc();
            //seta o progresso dessa etapa
//                 loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
            List<String> Tribunal = banco.SelectTribunal(reg, instancia);

            for (int i = 0; i < Tribunal.size(); i++) {
                banco.inserirConfig(nucleo, Tribunal.get(i), "ORG_JUGADOR");
            }
            System.gc();
            //seta o progresso dessa etapa
//                loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
            List<String> Especie = banco.SelectEspecie();

            for (int i = 0; i < Especie.size(); i++) {
                banco.inserirConfig(nucleo, Especie.get(i), "ESPECIE");
            }
            System.gc();
            //seta o progresso dessa etapa
//                 loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
            List<String> Entidade = banco.SelectEntidadeID();

            for (int i = 0; i < Entidade.size(); i++) {
                banco.inserirConfig(nucleo, Entidade.get(i), "ENTIDADE");
            }
            System.gc();
            //seta o progresso dessa etapa
//              loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
            List<String> Classe = banco.SelectClasse();

            for (int i = 0; i < Classe.size(); i++) {
                banco.inserirConfig(nucleo, Classe.get(i), "CLASSE");
            }
            System.gc();
            //seta o progresso dessa etapa
//             loadp.setProgresso();
            System.out.println("Acabei" + System.currentTimeMillis());
        } catch (Exception ex) {
            Logger.getLogger(ParamController.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {

            ObservableList<String> listR = FXCollections.observableArrayList();
            ObservableList<String> listI = FXCollections.observableArrayList();
            ObservableList<String> listP = FXCollections.observableArrayList();
            ObservableList<String> listC = FXCollections.observableArrayList();
            ResultSet regiaoR = Regiao.executeQuery();
            try {
                while (regiaoR.next()) {
                    listR.add(regiaoR.getString("REGIAO"));
                }
                Regiao.close();
                regiao.setItems(listR);
                listI.add("1");
                listI.add("2");
                instancia.setItems(listI);
                listP.clear();
                listP.add("0");
                listP.add("1");
                listP.add("2");
                listP.add("3");
                listP.add("4");
                listP.add("5");
                peso.setItems(listP);
                listC.clear();
                listC.add("0");
                listC.add("1");
                listC.add("2");
                listC.add("3");
                listC.add("4");
                listC.add("5");
                campos.setItems(listC);
            } catch (Exception ex) {
                Logger.getLogger(ParamController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParamController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
