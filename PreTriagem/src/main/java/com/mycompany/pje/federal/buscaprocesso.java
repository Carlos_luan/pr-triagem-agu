package com.mycompany.pje.federal;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author carlos.luan / eduardo.luiz
 */
public class buscaprocesso {

    public buscaprocesso(WebDriver driver) {
        super();
    }

    @SuppressWarnings("SleepWhileInLoop")
    public final boolean find(WebDriver driver, boolean antigo) {
        try {
            URL url = getClass().getResource("/SOUNDS/ability-gain.wav");
            AudioClip clip = Applet.newAudioClip(url);

            /*
* Foi necessário criar um delay entre a execução e o tempo de espera
* para carregar a pagina web
             */
            Actions action = new Actions(driver);
            Etiqueta etiquetar = new Etiqueta();
//transforma a tabela num webelement manipulavel

//localiza a tabela onde estão armazenadas as tarefas
            triagem triar = new triagem();
            WebElement tabela = driver.findElement(By.id("gridview-1109-table"));
//cria uma lista das tarefas na tela do usuário
            List<WebElement> tarefas = new ArrayList(tabela.findElements(By.cssSelector("tr")));
            String linha;
            BancoController banco = new BancoController();
            PreparedStatement nucleo = banco.SelectNucleo();
            ResultSet Nucleo = nucleo.executeQuery();
            String resultado;
            while (Nucleo.next()) {
                resultado = "";
                for (int i = 0; i < tarefas.size(); i++) {
                    linha = "";
                    List<WebElement> column = new ArrayList(tarefas.get(i).findElements(By.cssSelector("td")));
                    for (int j = 0; j < column.size(); j++) {
                        linha = linha + column.get(j).getText() + "  ";
                    }
                    resultado = triar.triar(Nucleo.getInt("CAMPOS"), Nucleo.getString("ETIQUETA"), linha, banco.SelectRelacionamentos(Nucleo.getString("ETIQUETA")));
                    etiquetar.exec_etiqueta(driver, resultado ,tarefas.get(i).getLocation());
                }
            }

            /*
* recebe o tamanho da lista, necessário para se realizar o laço e
* executar a triagem sem a necessidade de intervenção do cliente
             */
//                    Platform.runLater(new Runnable()
//                    {
//
//                        @Override
//                        public void run()
//                        {
//                            Alert erro = new Alert(Alert.AlertType.WARNING);
//                            erro.setTitle("Informação");
//                            erro.setHeaderText("Codigo: X002");
//                            erro.setContentText("Erro no processo de Triagem:\n");
//                            StringWriter sw = new StringWriter();
//                            PrintWriter pw = new PrintWriter(sw);
//                            ex.printStackTrace(pw);
//                            String exceptionText = sw.toString();
//
//                            Label label = new Label("The exception stacktrace was:");
//
//                            TextArea textArea = new TextArea(exceptionText);
//                            textArea.setEditable(false);
//                            textArea.setWrapText(true);
//
//                            textArea.setMaxWidth(Double.MAX_VALUE);
//                            textArea.setMaxHeight(Double.MAX_VALUE);
//                            GridPane.setVgrow(textArea, Priority.ALWAYS);
//                            GridPane.setHgrow(textArea, Priority.ALWAYS);
//
//                            GridPane expContent = new GridPane();
//                            expContent.setMaxWidth(Double.MAX_VALUE);
//                            expContent.add(label, 0, 0);
//                            expContent.add(textArea, 0, 1);
//
//// Set expandable Exception into the dialog pane.
//                            erro.getDialogPane().setExpandableContent(expContent);
//                            erro.showAndWait();
//                        }
//                    });
//                        return true;
//                    }
////                }
////                else
////                {
////                    Etiqueta etiqueta1 = new Etiqueta();
////                    etiqueta1.exec_etiqueta(driver, "MATERIA NÃO PREVIDENCIARIA", "", "");
////                }
//
//            }
//            catch (Exception e)
//            {
//                clip.play();
//                int op = JOptionPane.showConfirmDialog(null, "Demora no tempo de resposta do Sapiens\nDeseja continuar triando?", "Triagem encerrada", JOptionPane.YES_NO_OPTION);
//                if (op == 0)
//                {
//                    Thread.sleep(2000);
//                }
//                else
//                {
//                    return false;
//                }
//            }
//
//        }
        } catch (Exception ex) {
            Logger.getLogger(buscaprocesso.class.getName()).log(Level.SEVERE, null, ex);

        }
        return false;
    }
}
