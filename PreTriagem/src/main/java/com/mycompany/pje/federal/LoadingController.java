/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextArea;

import javafx.fxml.FXML;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javax.sound.midi.MidiDevice.Info;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

/**
 *
 * @author Edu
 */
public class LoadingController implements Initializable {

    double i;
    boolean completo = false;
    @FXML
    JFXTextArea InfoTxt;
    @FXML
    JFXButton fecharBtn;
    @FXML
    JFXProgressBar BarraLoad;
    
    public void setProgresso() {
        BarraLoad.setProgress(1 / 7);
    }

    public void runTask() throws InterruptedException {

        do {
            BarraLoad.getProgress();
            Thread.sleep(10000);
        } while (BarraLoad.getProgress() <= 1);
        InfoTxt.setText("Configuração Concluída!");
        fecharBtn.setVisible(true);
    }

    @FXML
    public void fechar(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        Parent root = null;
        stage.close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Create a Runnable  
        Runnable task = new Runnable() {
            public void run() {
                try {
                    runTask();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };

        // Run the task in a background thread
        Thread backgroundThread = new Thread(task);
        // Terminate the running thread if the application exits
        backgroundThread.setDaemon(true);
        // Start the thread
        backgroundThread.start();
    }
}
