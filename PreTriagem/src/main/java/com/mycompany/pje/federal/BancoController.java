/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlo
 */
public class BancoController {

    public Connection conectar() {
        try {
            Connection conectar = DriverManager.getConnection("jdbc:sqlite:Pre_triagem.db");
            return conectar;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean inserirNucleo(String Etiqueta, int peso, int regiao, int instancia, int campos) {
        try {
            Connection conectar = conectar();
            Statement statement = conectar.createStatement();
            statement.execute("INSERT INTO NUCLEO (ETIQUETA,REGIAO,PESO,INSTANCIA,CAMPOS)\n"
                    + "VALUES ('" + Etiqueta + "','" + regiao + "','" + peso + "','" + instancia + "','" + campos + "');");
            return true;
        } catch (Exception ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void inserirConfig(String nucleo, String ID, String table) {
        try {
            Connection conectar = conectar();
            Statement statement = conectar.createStatement();
            statement.execute("INSERT INTO " + table + "_NUCLEO (ID_NUCLEO,ID)" + "VALUES ('" + nucleo + "','" + ID + "')");
            statement.close();
            conectar.close();
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ResultSet> SelectRelacionamentos(String Nucleo) {
        try {
            List<ResultSet> rquery = null;
            Connection conectar = conectar();

            PreparedStatement assunto = conectar.prepareStatement("SELECT *FROM ASSUNTO_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' and [CHECK] = [1]");
            PreparedStatement classe = conectar.prepareStatement("SELECT *FROM CLASSE_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' [CHECK] = [1]");
            PreparedStatement entidade = conectar.prepareStatement("SELECT *FROM ENTIDADE_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' [CHECK] = [1]");
            PreparedStatement especie = conectar.prepareStatement("SELECT *FROM ESPECIE_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' [CHECK] = [1]");
            PreparedStatement org_jugador = conectar.prepareStatement("SELECT *FROM ORG_JUGADOR_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' [CHECK] = [1]");
            PreparedStatement unidade = conectar.prepareStatement("SELECT *FROM UNIDADE_NUCLEO WHERE ID_NUCLEO='" + Nucleo + "' [CHECK] = [1]");

            rquery.add(assunto.executeQuery());
            rquery.add(classe.executeQuery());
            rquery.add(entidade.executeQuery());
            rquery.add(especie.executeQuery());
            rquery.add(org_jugador.executeQuery());
            rquery.add(unidade.executeQuery());

            return rquery;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // rquery -> armazena o resultado da query feita no banco de dados
    public List SelectAssunto() {
        try {
            List<String> Assunto = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ASSUNTO");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Assunto.add(rquery.getString("ASSUNTO"));
            }
            rquery.close();
            stmt.close();
            conectar.close();

            return Assunto;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectAssuntoID() {
        try {
            List<String> Assunto = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ASSUNTO");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Assunto.add(rquery.getString("ID"));
            }
            rquery.close();
            stmt.close();
            conectar.close();

            return Assunto;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PreparedStatement SelectRegiao() {
        try {
            List<String> Regiao = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM REGIAO");
            return stmt;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PreparedStatement SelectRegiaoSet(String regiao) {
        try {
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM REGIAO WHERE REGIAO= '" + regiao + "'");
            // rquery = stmt.executeQuery()
            return stmt;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public PreparedStatement SelectNucleo() {
        try {
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM NUCLEO ORDER BY PESO DESC");
            return stmt;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectBanco(String tabela) {
        try {
            List<String> Classe = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM "+tabela);
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Classe.add(rquery.getString(tabela));
            }
            return Classe;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectClasse() {
        try {
            List<String> Classe = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM CLASSE");
            rquery = stmt.executeQuery();
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Classe.add(rquery.getString("CLASSE"));
            }
            return Classe;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectEntidade() {
        try {
            List<String> Entidade = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ENTIDADE ");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Entidade.add(rquery.getString("ENTIDADE"));
            }
            conectar.close();
            return Entidade;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectEntidadeID() {
        try {
            List<String> Entidade = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ENTIDADE");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Entidade.add(rquery.getString("ID"));
            }
            conectar.close();
            return Entidade;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectEspecie() {
        try {
            List<String> Especie = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ESPECIE");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Especie.add(rquery.getString("ID"));
            }
            conectar.close();
            return Especie;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List SelectTribunal(int regiao, String instancia) {
        try {
            List<String> Tribunal = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM ORG_JUGADOR WHERE REGIAO = " + regiao + " and INSTANCIA = " + instancia);
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Tribunal.add(rquery.getString("ID"));
            }
            conectar.close();
            return Tribunal;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<String> SelectUnidade() {
        try {
            List<String> Unidade = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM UNIDADE");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Unidade.add(rquery.getString("UNIDADE"));
            }
            conectar.close();
            return Unidade;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<String> SelectUnidadeID() {
        try {
            List<String> Unidade = new ArrayList();
            ResultSet rquery = null;
            Connection conectar = conectar();
            PreparedStatement stmt = conectar.prepareStatement("SELECT *FROM UNIDADE");
            rquery = stmt.executeQuery();
            while (rquery.next()) {
                Unidade.add(rquery.getString("ID"));
            }
            conectar.close();
            return Unidade;
        } catch (SQLException ex) {
            Logger.getLogger(BancoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
