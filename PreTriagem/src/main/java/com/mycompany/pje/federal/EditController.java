/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pje.federal;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.controlsfx.control.CheckListView;

/**
 *
 * @author carlo
 */
public class EditController implements Initializable {
        
    
     private Chaves chave;

    public EditController(Chaves chave)
    {
        this.chave = chave;
    }
    
    @FXML
    JFXListView Colunas;
    @FXML
    JFXListView Variaveis;
    @FXML
    JFXListView Regras;

    @FXML   
    public void selecionarColuna(){
        BancoController banco = new BancoController();
        List <String> tabela = banco.SelectBanco(Colunas.getSelectionModel().getSelectedItem().toString().replace(".","_"));
         ObservableList<String> Variavel = FXCollections.observableArrayList();
         Variavel.addAll(tabela);
         Variaveis.setItems(Variavel);
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> Coluna = FXCollections.observableArrayList();
        Coluna.add("ASSUNTO");
        Coluna.add("CLASSE");
        Coluna.add("ENTIDADE");
        Coluna.add("ESPECIE");
        Coluna.add("ORG.JULGADOR");
        Coluna.add("UNIDADE");        
        Colunas.setItems(Coluna);
    }

}
